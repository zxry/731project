// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-analytics.js";
import { getfirestore } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-firestore.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDSFrzD61NdYkehrWAZGQ8Ng9NlqDqcsHg",
  authDomain: "project-4dda0.firebaseapp.com",
  projectId: "project-4dda0",
  storageBucket: "project-4dda0.appspot.com",
  messagingSenderId: "646632568711",
  appId: "1:646632568711:web:524f1e40ffdc534ed98522",
  measurementId: "G-FLFD0FK14F"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

export const db = getfirestore(app);
