import {db } from "/js/firebase.js";
import { doc, setDoc, deleteDoc, collection, getDocs } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-firestore.js"; 

$(function(){
    console.log('Script is running');
    $('#loginform').hide();
    $('#signinform').hide();
    $('#courseAdder').hide();
    $('#courseRemover').hide();
    $('#coursegoal').hide();

    var fname = "John";
    var lname = "Doe";
    var pw = "123123";
    var sn = "45678";

    var docData = {
        fname: fname,
        lname: lname,
        pw: pw,
        sn: sn
    }

    setDoc(doc(db, "users", sn), docData);


    $('#loginbtn').click(function(){
        console.log('LOGIN BUTTON IS CLICKED');
        $('#loginform').show();
        $('#signinform').hide();
    });

    $('#signupbtn').click(function(){
        console.log('SIGNUP BTN IS CLICKED');
        $('#signinform').show();
        $('#loginform').hide();
    });
    
    $('#login').click(function(){
        window.open('/Main/home.html');
    });
    
    $('#logoutbtn').click(function(){
        window.open('/Main/sample.html');
    });

    //Add courses
    $('#add').click(function(){
        if($('#courseAdder').is(":visible")){
            $('#courseAdder').hide();
        }
        else{
            $('#courseAdder').show();
            $('#courseRemover').hide();
            $('#coursegoal').hide();
        }
    });

    $('#addCourseBtn').click(function(){
        //$('#courseTable tr:last').after('<tr><td><b>'+$("#courseCode1").val()+'</b></td><td>'+$("#courseAve").val()+'</td><td>A</td></tr>')
        var code = $("#courseCode1").val();
        var ave = $("#courseAve").val();
        var letter = convertToLetterGrade(ave);
        var htmlString = '<tr id=course'+code+'><td>'+code+'</td><td>'+ave+'%</td><td>'+letter+'</td><td><button class="btn btn-outline-danger" id="removebtn">Remove</button></td></tr>';
        $('#courseTable').append(htmlString);
    })

    //Remove courses
    $('#remove').click(function(){
        if($('#courseRemover').is(":visible")){
            $('#courseRemover').hide();
        }
        else{
            $('#courseRemover').show();
            $('#courseAdder').hide();
            $('#coursegoal').hide();
        }        
    })

    $('#removeCourseBtn').click(function(){
        var input = $('#courseCode2').val();
        removeCourse(input);
    })

    $('#courseTable').on('click', '#removebtn', function(){
        $(this).closest('tr').remove();
    });

    //Goal Average
    $('#goal').click(function(){
        if($('#coursegoal').is(":visible")){
            $('#coursegoal').hide();
        }
        else{
            $('#coursegoal').show();
            $('#courseAdder').hide();
            $('#courseRemover').hide();
        }        
    })
            
    $('#goalAveBtn').click(function(){
        $('table thead th').each(function(index) {
            courseSum(index)
        });
    });
        
});

function courseSum(index){
    var total = 0;
    $("td:nth-child(2)").each(function(){
        var value = parseInt($(this).text().replace(" ","").replace(",-", ""));
        if (!isNaN(value)) {
            total += value;
        }    
    });
    var input1 = parseInt($('#courseCode3').val());
    var rowCount = parseInt($('#courseTable tr').length);
    var goalOut = (input1 * rowCount - total);
    $('#result').text('The average needed is: ' + goalOut +'%').wrapInner("<strong />");
};

function convertToLetterGrade(ave){
    var letterGrade;
    switch(true){
        case (ave <= 100 && ave >= 90):
            letterGrade = 'A+';
            break;
        case (ave <= 89 && ave >= 85):
            letterGrade = 'A';
            break;
        case (ave <= 84 && ave >= 80):
            letterGrade = 'A-';
            break;
        case (ave <= 79 && ave >= 77):
            letterGrade = 'B+';
            break;
        case (ave <= 76 && ave >= 73):
            letterGrade = 'B';
            break;
        case (ave <= 72 && ave >= 70):
            letterGrade = 'B-';
            break;
        case (ave <= 69 && ave >= 67):
            letterGrade = 'C+';
            break;
        case (ave <= 66 && ave >= 63):
            letterGrade = 'C';
            break;
        case (ave <= 62 && ave >= 60):
            letterGrade = 'C-';
            break;
        case (ave <= 59 && ave >= 57):
            letterGrade = 'D+';
            break;
        case (ave <= 56 && ave >= 53):
            letterGrade = 'D';
            break;
        case (ave <= 52 && ave >= 50):
            letterGrade = 'D-';
            break;    
        case (ave <= 49 && ave >= 0):
            letterGrade = 'F';
            break;
        case (ave > 100 && ave < 0):
            letterGrade = 'INVALID AVERAGE';
            break;
        default:
           return 'INVALID AVERAGE';
       }
       return letterGrade;
    }

function removeCourse(code){
    $('#course'+code).remove();
}